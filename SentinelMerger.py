# Import raster libraries
import rasterio
# Library for selecting folders
from tkinter.filedialog import askdirectory
# Native Python libraries
import os

# Asks for a folder path
path = askdirectory(title='Select the folder that contains the bands.')
path = path + '/'

# Reads all files on that path
entries = os.listdir(path)

# Saves all files on a list
file = [];
for entry in entries:
	file.append(entry);

# Saves only the files with .tiff or .jp2 extension
res = [];
for i in range(len(file)):
	if file[i].find('.tiff') != -1 or file[i].find('.jp2') != -1:
		res.append(file[i])
res.sort()

# Shows loaded bands
print("The merged bands are (in this order):")
for i in range(len(res)):
	print(res[i][0:3])

# Loads bands in memory
loadedbands = 0;
b = [];
for i in range(len(res)):
	b.append(rasterio.open(path + res[i], driver='Gtiff'))
	loadedbands = loadedbands + 1;

# Tries until finds data
numcolum = 0;
for i in range(loadedbands):
	if numcolum == 0:
		# Image size
		numcolum = b[i].width
		numfiles = b[i].height
		datatype = b[1].dtypes[0]
		# Reference system
		sisref = b[i].crs
		# Transformation parameters
		transform = b[i].transform
		break

# Merges bands and save them on a file
mergedbands = rasterio.open(path + 'MERGED' + res[0][3:],'w',driver='Gtiff',
                         width=numcolum, height=numfiles,
                         count=loadedbands,
                         crs=sisref,
                         transform=transform,
                         dtype=datatype
                         )

# Saves on a file
for i in range(loadedbands):
	mergedbands.write(b[i].read(1),i+1)
mergedbands.close()
print("Finished.")