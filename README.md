# Sentinel 2 bands merger

## Brief description
*The [Sentinel-2 Global Mosaic (S2GM)](https://s2gm.sentinel-hub.com/) service is a component of the Copernicus Global Land Service providing composites from time-series of Sentinel-2 surface reflectance observations.* 
The S2GM has a tool called [Sentinel 2 Mosaic Hub](https://apps.sentinel-hub.com/mosaic-hub/) that allows the user to download Sentinel 2 images from a definite area and date.
When you download some specific data each band downloaded corresponds to one different file:

| Band | Associated file |
| :------: | :------: |
| Red | B02_Y10_20200101_ExampleZone.tiff |
| Green | B03_Y10_20200101_ExampleZone.tiff |
| Blue | B04_Y10_20200101_ExampleZone.tiff |

Since this file format can be unpractical I wrote this program to get one multispectral image from this downloaded bands.

## How to use
You must have installed [Python](https://www.python.org/downloads/) on your computer to use this script.
1.  Download an image from the [Sentinel 2 Mosaic Hub](https://apps.sentinel-hub.com/mosaic-hub/).
    * Image format: Only .tiff or .jp2 formats. NetCDF is not allowed.
    * Resolution: 10m/30m/60m.
    * Bitdepth: Full or reduced
    * Coordinate system: UTM or WGS84.
2.  Run the script with Python GUI or code editor like Sublime Text 3 or Atom.
3.  Select the folder that contains the downloaded bands.
4.  That's it. The multispectral image (multibands image) is on the same folder as the downloaded data.
### Dependencies
This programs needs some libraries to work:
*  [rasterio](https://rasterio.readthedocs.io/en/latest/): To process images. Installation needed.
*  tkinter: To select folder. No installation should be needed.
*  os: Te read files. No installation should be needed.

They appear on the script as:
```python
# Import raster libraries
import rasterio
# Library for selecting folders
from tkinter.filedialog import askdirectory
# Native Python libraries
import os
```

## Works on
* Windows 10 with Python 3.8.2, last test on March 2020.
* Ubuntu 19.10 with Python 3.8, last test on March 2020.

## Upcoming features
* GUI (graphical user interface).
* Minimal version.
* Standalone Windows executable file (.exe).
* Standalone Linux file.
 

**Still improving...**